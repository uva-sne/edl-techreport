\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{snetr}[2008/01/31 Version 1.0 SNE Technical Report]
\RequirePackage{graphicx}
\RequirePackage{tabularx}
\RequirePackage{fancyhdr}
\RequirePackage{url}
\LoadClass[11pt,a4paper]{article}

% Copyright (c) 2007, Jeroen van der Ham, System- and Network Engineering group, UvA
% 
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
%     * Redistributions of source code must retain the above copyright notice,
%       this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of SNE UvA nor the names of its contributors
%       may be used to endorse or promote products derived from this software
%       without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
% CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
% EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
% PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
% PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
% LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
% NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


% \pdfpagewidth 21cm
% \pdfpageheight 29.7cm

\newif\if@abstract
\@abstractfalse
\renewcommand{\abstract}[1]{\@abstracttrue\gdef\@abstract{#1}}

\newif\if@SNEreport
\@SNEreportfalse
\newcommand{\SNEreport}[2]{\@SNEreporttrue\gdef\@SNEreport{SNE-UVA-#1-#2}\gdef\@SNEyear{20#1}}

\renewcommand{\maketitle}{%
        \null
        % Redefine the style of this page
        \thispagestyle{fancy}
        % Save the values of these heights, so that we can restore them later.
        \edef\saveheadheight{\the\headheight}
        \edef\saveheadwidth{\the\headwidth}
        \edef\savefootskip{\the\footskip}
        % no line for the header.
        \renewcommand{\headrulewidth}{0pt}
        % Just a thin line for the footer.
        \renewcommand{\footrulewidth}{0.4pt}
        % Raise the footer so that we don't have problems with printer margins.
        \footskip 0pt
        % Make the header larger to accomodate the images.
        \headheight 75pt
        \headwidth 210mm
        % We create a boxed tabularx so that the text are centered beneath the logos, and the texts are aligned. 
        % We use the left head so that it aligns properly on the page (chead didn't want to go left enough)
        \newsavebox{\logos}
        \savebox{\logos}{\begin{tabularx}{160mm}{cXc}\includegraphics[height=20mm]{uvalogo}& &\includegraphics[height=18mm]{snelogo}\\\textsc{Universiteit van Amsterdam} & & \textsf{\large System and Network Engineering}\\\end{tabularx}}
        \lhead{\usebox{\logos}}
        % We position the lhead somewhat more to the left so that it is centered on the page. (positive is left, negative is right)
        % Note that changes to the width of the table should also be added/removed (divided by two) to the this length.
        \fancyheadoffset{17mm}
        % Title, author and date.
        \vskip 4cm
         \begin{center}\leavevmode
           \normalfont
           {\huge\sffamily\bfseries \@title\par}%
           \vskip 5mm
           {\large\itshape \@author\par}%
           \vskip 5mm
           {\large\itshape \@date\par}%
         \vfill
         % If there is an abstract, we print it here
         \if@abstract
           \begin{minipage}{.8\linewidth}
               \noindent\large\textbf{Abstract}\par\setlength{\parindent}{2em}\noindent\itshape\@abstract
           \end{minipage}
         \fi
         \vskip 2.5cm
         \end{center}%
         % The footer contains the report number, and a reference to the SNE Reports website.
         \cfoot{\large SNE technical report \@SNEreport\\\normalsize\url{http://www.science.uva.nl/research/sne/reports/}}
         % Break to a new page, and restore the values that we saved in the beginning.
         \newpage
         \headheight\saveheadheight
         \headwidth\saveheadwidth
         \footskip\savefootskip

}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%% Sans Serif font voor sections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\section}{\@startsection {section}{1}{\z@}%
        {-3.5ex \@plus -1ex \@minus -.2ex}%
        {2.3ex \@plus.2ex}%
        {\reset@font\Large\bfseries\sffamily}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{\z@}%
        {-3.25ex\@plus -1ex \@minus -.2ex}%
        {1.5ex \@plus .2ex}%
        {\reset@font\large\bfseries\sffamily}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{\z@}%
        {-3.25ex\@plus -1ex \@minus -.2ex}%
        {1.5ex \@plus .2ex}%
        {\reset@font\normalsize\bfseries\sffamily}}
\renewcommand{\paragraph}{\@startsection{paragraph}{4}{\z@}%
        {3.25ex \@plus1ex \@minus.2ex}%
        {-1em}%
        {\reset@font\normalsize\bfseries}}
\renewcommand{\subparagraph}{\@startsection{subparagraph}{5}{\parindent}%
        {3.25ex \@plus1ex \@minus .2ex}%
        {-1em}%
        {\reset@font\normalsize\bfseries}}
        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Redefine the caption command to center and italics.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \centering\itshape #1:\hspace{.5em}#2
  \vskip\belowcaptionskip}